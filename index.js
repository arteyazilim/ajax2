(function(root, factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define('Ajax', ['qw'], factory);
    } else if (typeof module === 'object' && typeof module.exports === 'object') {
        module.exports = factory(require('qw'));
    } else {
        root.Ajax = factory(root.qw);
    }
})(this, function(qw) {
    'use strict';

    var rClearQuery = /(?:^[\?&]+|[\?&]+$)/g;

    function console_error(message)
    {
        if ('console' in window && typeof console.log != 'undefined') {
            console.log('Error: ' + message);
        }
    }

    function obj2Query(obj)
    {
        if (qw.isString(obj)) {
            return obj;
        } else if (!qw.isPlainObject(obj)) {
            return null;
        }
        var result = [];
        qw.each(obj, function(val, key) {
            result.push(encodeURIComponent(key) + '=' + encodeURIComponent(val));
        });
        if (result.length) {
            return result.join('&');
        }
        return null;
    }

    function add2Url(url, data)
    {
        var append = obj2Query(data), glue;
        url = qw.isString(url) ? url.replace(rClearQuery, '') : '';
        if (!append) {
            return url;
        }
        append = append.replace(rClearQuery, '');
        glue = url.indexOf('?') === -1 ? '?' : '&';
        return url + glue + append;
    }

    function add2Query(query, data)
    {
        var append = obj2Query(data);
        query = qw.isString(query) ? query.replace(rClearQuery, '') : '';
        if (!append) {
            return query || null;
        }
        append = append.replace(rClearQuery, '');
        return (query ? query + '&' : '') + append;
    }

    function fixHeader(header)
    {
        if (!qw.isString(header)) {
            return header;
        }
        return header.toLowerCase().replace(/(?:^|\-)[a-z]/g, function(all, match) {
            return all.toUpperCase();
        });
    }

    function readHeaders(headerText)
    {
        var headers = {};
        qw.each(headerText.split("\r\n"), function(header) {
            var parts;
            header = qw.trim(header);
            if (header.indexOf(':') > 0) {
                parts = header.split(':');
                headers[ fixHeader(parts[ 0 ]) ] = qw.trim(parts[ 1 ]);
            }
        });
        return headers;
    }

    function buildResponseObject(xhr, start, why)
    {
        var _xhr = xhr;
        var _why = why;
        var _isXDR = false;
        var _status = 0;
        var _text = null;
        var _json = null;
        var _error = null;
        var _headers = null;
        var _response_type = null;
        var _start_time = start;
        var _total_time = start - qw.now();

        // XDomainRequest mi?
        if (typeof XDomainRequest != 'undefined' && xhr instanceof XDomainRequest) {
            _isXDR = true;
        }

        var _fetchHeaders = function() {
            return null;
        };

        if (_why === Ajax.TIMEOUT) {
            _status = -1;
            _error = 'timeout';
        } else if (_why === Ajax.ERROR) {
            _status = 0;
            _error = 'request error';
        } else if (_isXDR && 'responseText' in that._xhr && typeof _xhr.responseText == 'string') {
            // eğer XDomainRequest ise responseText varsa status ve headersi elle belirliyoruz.
            // XDomainRequest bu verileri erişilmesini engelliyor.
            _status = 200;
            _error = '';
        } else {
            _status = _xhr.status || 0;
            _fetchHeaders = function() {
                if (_headers !== null) {
                    return _headers;
                }
                _headers = readHeaders(_xhr.getAllResponseHeaders());
                return _headers;
            };
        }

        var _isJsonResponse = function() {
            if (_response_type !== null) {
                return _response_type === 'json';
            }
            _response_type = 'text';
            var headers = _fetchHeaders();
            if (headers && ('Content-Type' in headers)) {
                var ctype = headers['Content-Type'];
                if (/^(application|text)\/(?:x\-|)json/i.test(ctype)) {
                    _response_type = 'json';
                }
            }
            return _response_type === 'json';
        };

        var _fetchResponseText = function() {
            if (_text !== null) {
                return _text;
            }
            _text = 'responseText' in _xhr ? _xhr.responseText : '';
            return _text;
        };

        var _fetchResponseJSON = function() {
            if (_json !== null) {
                return _json;
            }
            var text = _fetchResponseText();
            try {
                _json = JSON.parse(text);
            } catch (e) {
                console_error('json parse error.');
                _json = null;
            }
            return _json;
        };

        var _fetchErrorMessage = function() {
            if (_error !== null) {
                return _error;
            }
            if (_status >= 200 && _status < 300) {
                _error = '';
            } else if (_isJsonResponse()) {
                _error = 'statusText' in _xhr ? _xhr.statusText : 'unknown error';
            } else {
                _error = _fetchResponseText() || ('statusText' in _xhr ? _xhr.statusText : 'unknown error');
            }
            return _error;
        };

        return {
            xhr: function() {
                return _xhr;
            },
            status: function() {
                return _status;
            },
            headers: function() {
                return _fetchHeaders();
            },
            get: function() {
                return _isJsonResponse() ? this.json() : this.text();
            },
            text: function() {
                return _fetchResponseText();
            },
            json: function() {
                return _fetchResponseJSON();
            },
            success: function() {
                return _why === Ajax.RESPONSE && _status >= 200 && _status < 300;
            },
            error: function() {
                return _fetchErrorMessage();
            }
        };
    }

    function XHRRequest()
    {
        if (!(this instanceof XHRRequest)) {
            return new XHRRequest();
        }
        this._xhr = new XMLHttpRequest();
        this._async = true;
        this._method = 'GET';
        this._url = '';
        this._data = null;
        this._timeout = -1;
        this._headers = null;
        this._start_time = 0;
        this._credentials = false;

        this._onresponse = null;
        this._onabort = null;
        this._aborted = false;
        this._timeout_occured = false;
    }

    XHRRequest.prototype.send = function()
    {
        var testUrl = this._url.toLowerCase(), cors = true;
        if (
            // TAM URL değilse
            !/^(?:https?\:)?\/\//i.test(testUrl) ||
            // "//domain.tld"
            testUrl.indexOf('//' + window.location.host) === 0 ||
            // "http://domain.tld"
            testUrl.indexOf('http://' + window.location.host) === 0 ||
            // "https://domain.tld"
            testUrl.indexOf('https://' + window.location.host) === 0
        ) {
            cors = false;
        }

        var isXDomainRequest = false;
        if (this._cors) {
            if (this._credentials && !('withCredentials' in this._xhr) && typeof XDomainRequest != 'undefined') {
                this._xhr = new XDomainRequest();
                isXDomainRequest = true;
            }
        }

        // bağlantı açılıyor.
        this._xhr.open(this._method, this._url, this._async);

        // credentials ayarlanıyor
        if (this._credentials) {
            if ('withCredentials' in this._xhr || isXDomainRequest) {
                this._xhr.withCredentials = true;
            } else {
                console_error('Cross-origin request is not supported.');
            }
        }

        // cors false ise ve header varsa ekleniyor.
        if(!cors && qw.isPlainObject(this._headers)) {
            qw.each(this._headers, function(val, key) {
                this._xhr.setRequestHeader(key, val);
            }, this);
        }

        // data varsa ve method uygunsa
        if (this._data && (this._method !== 'GET' && this._method !== 'HEAD')) {
            this._xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        }

        var that = this;
        // eğer eski XMLHttpRequest ise
        if (!('withCredentials' in this._xhr) && this._xhr instanceof XMLHttpRequest) {
            this._xhr.ontimeout = function () {
                that._timeout_occured = true;
                if (that._onresponse) {
                    that._onresponse(buildResponseObject(that._xhr, that._start_time, Ajax.TIMEOUT));
                }
            };
            this._xhr.onreadystatechange = function () {
                if (that._xhr.readyState === 4 && !that._timeout_occured && !that._aborted && that._onresponse) {
                    // başarılı response durumu
                    that._onresponse(buildResponseObject(that._xhr, that._start_time, Ajax.RESPONSE));
                }
            };
        } else {
            this._xhr.onerror = function () {
                if (!that._aborted && that._onresponse) {
                    that._onresponse(buildResponseObject(that._xhr, that._start_time, Ajax.ERROR));
                }
            };
            this._xhr.ontimeout = function () {
                that._timeout_occured = true;
                if (that._onresponse) {
                    that._onresponse(buildResponseObject(that._xhr, that._start_time, Ajax.TIMEOUT));
                }
            };
            if ('onabort' in this._xhr) {
                this._xhr.onabort = function () {
                    if (that._onabort) {
                        that._onabort(that);
                    }
                };
            }
            this._xhr.onload = function () {
                if (!that._timeout_occured && !that._aborted && that._onresponse) {
                    that._onresponse(buildResponseObject(that._xhr, that._start_time, Ajax.RESPONSE));
                }
            };
        }
        if (qw.isNumber(this._timeout) && this._timeout > 0) {
            this._xhr.timeout = this._timeout * 1000;
        }
        // IE garbage collector, xdr asenkron olarak send edildiğinde
        // send işlemi gerçekleşmeden xdr'yi scope bittiği için kaldırıyor.
        // bu sorunun önüne geçmek için xdr global'de saklanmalı.
        // http://stackoverflow.com/a/27583212
        // if (isXDomainRequest && this._async) {}
        // şimdilik biz bu sorunla karşılaşmadık.

        this._xhr.send(this._data);
        this._start_time = qw.now();
    };

    XHRRequest.prototype.getXhr = function()
    {
        return this._xhr;
    };

    XHRRequest.prototype.setAsync = function(async)
    {
        this._async = !!async;
    };

    XHRRequest.prototype.isAsync = function()
    {
        return this._async;
    };

    XHRRequest.prototype.setTimeout = function(timeout)
    {
        this._timeout = qw.isNumber(timeout) && timeout > 0 ? timeout : -1;
    };

    XHRRequest.prototype.getTimeout = function()
    {
        return this._timeout;
    };

    XHRRequest.prototype.setMethod = function(method)
    {
        this._method = method.toUpperCase();
    };

    XHRRequest.prototype.getMethod = function()
    {
        return this._method;
    };

    XHRRequest.prototype.setUrl = function(url)
    {
        this._url = url;
    };

    XHRRequest.prototype.getUrl = function()
    {
        return this._url;
    };

    XHRRequest.prototype.setData = function(data)
    {
        if (data && !qw.isString(data)) {
            console_error('data must be in string format');
            data = null;
        }
        this._data = data;
    };

    XHRRequest.prototype.getData = function()
    {
        return this._data;
    };

    XHRRequest.prototype.withCredentials = function(credentials)
    {
        this._credentials = !!credentials;
    };

    XHRRequest.prototype.isWithCredentials = function()
    {
        return this._credentials;
    };

    XHRRequest.prototype.addHeader = function(name, value)
    {
        if (!qw.isPlainObject(this._headers)) {
            this._headers = {};
        }
        this._headers[ fixHeader(name) ] = value;
    };

    XHRRequest.prototype.getHeaders = function()
    {
        return this._headers;
    };

    XHRRequest.prototype.getTime = function()
    {
        return this._start_time;
    };

    XHRRequest.prototype.abort = function()
    {
        var fuxie = false; // what the fux is that ie?
        if (!('onabort' in this._xhr)) {
            fuxie = true;
        }
        this._aborted = true;
        this._xhr.abort();
        if (fuxie && this._onabort) {
            var that = this;
            setTimeout(function() {
                that._onabort(that);
            }, 0);
        }
    };

    XHRRequest.prototype.onResponse = function(fn)
    {
        this._onresponse = qw.isFunction(fn) ? fn : null;
    };

    XHRRequest.prototype.onAbort = function(fn)
    {
        this._onabort = qw.isFunction(fn) ? fn : null;
    };

    var defaultOptions = {
        method: 'GET',
        url: '',
        data: null,
        dataType: 'text',
        timeout: -1,
        cache: false,
        headers: {'X-Requested-With': 'XMLHttpRequest'},
        cacheQueryKey: null
    }

    function Ajax(options)
    {
        var _request = new XHRRequest();
        var _options = qw.clone(defaultOptions);

        if (window.defaultAjaxOptions && qw.isPlainObject(window.defaultAjaxOptions)) {
            qw.extend(true, _options, window.defaultAjaxOptions);
        }
        if (options && qw.isPlainObject(options)) {
            qw.extend(true, _options, options);
        }

        // method ayarlanıyor.
        _request.setMethod(_options.method);

        if (qw.isPlainObject(_options.data)) {
            // data object ise stringe çeviriliyor.
            // null ya da dolu string döndürür.
            _options.data = obj2Query(_options.data);
        } else if (qw.isString(_options.data)) {
            // string ise temizleniyor.
            _options.data = _options.data.replace(rClearQuery, '');
        } else {
            // diğer ihtimallerin hepsinde null yapılır.
            _options.data = null;
        }

        if (!_options.url) {
            _options.url = window.location.href;
        }

        // data varsa ve method GET|HEAD ise data urlye ekleniyor.
        if (_options.data && /^(?:GET|HEAD)$/i.test(_options.method)) {
            _options.url = add2Url(_options.url, _options.data);
            // bu noktada data yokmuş gibi davranılacak.
            _options.data = null;
        }


        if (!_options.cache) {
            var value = (qw.now() + '').substring(6) + qw.createRandom();
            var key = qw.isString(_options.cacheQueryKey) ? _options.cacheQueryKey : '__no_cache';
            _options.url = add2Url(_options.url, key + '=' + value);
        }

        // URL ayarlanıyor.
        _request.setUrl(_options.url);

        // data ayarlanıyor.
        if (_options.data) {
            _request.setData(_options.data);
        }

        // timeout ayarlanıyor.
        if (qw.isNumber(_options.timeout) && _options.timeout > 0) {
            _request.setTimeout(_options.timeout);
        }

        // headerlar ekleniyor.
        if (qw.isPlainObject(_options.headers)) {
            qw.each(_options.headers, function(val, key) {
                this.addHeader(key, val);
            }, _request);
        }

        var dataType = 'text';
        if (_options.dataType.toLowerCase() === 'json') {
            dataType = 'json';
        } else if (_options.dataType.toLowerCase() === 'xml') {
            dataType = 'xml';
        }

        var responseEvents = [];
        _request.onResponse(function(response) {
            qw.each(responseEvents, function(fn) {
                fn(response);
            });
        });

        var abortEvents = [];
        _request.onAbort(function() {
            qw.each(abortEvents, function(fn) {
                fn(_request);
            });
        });

        // istek gönderiliyor.
        _request.send();

        return {
            done: function (fn) {
                responseEvents.push(function(response) {
                    if (response.success()) {
                        fn(dataType === 'json' ? response.json() : response.text(), _request, response);
                    }
                });
                return this;
            },
            error: function (fn) {
                responseEvents.push(function(response) {
                    if (!response.success()) {
                        fn(response.error(), _request, response);
                    }
                });
                return this;
            },
            always: function (fn) {
                responseEvents.push(function(response) {
                    fn(dataType === 'json' ? response.json() : response.text(), _request, response);
                });
                return this;
            },
            abort: function () {
                if (arguments.length === 0) {
                    _request.abort();
                } else {
                    var fn = arguments[ 0 ];
                    abortEvents.push(function(request) {
                        fn(request);
                    });
                }
                return this;
            }
        };
    }

    Ajax.ERROR = -1;
    Ajax.TIMEOUT = -2;
    Ajax.RESPONSE = -3;

    Ajax.get = function(url, options)
    {
        if (!qw.isPlainObject(options)) {
            options = {};
        }
        options.url = url;
        options.method = 'GET';
        return Ajax(options);
    };

    Ajax.post = function(url, options)
    {
        if (!qw.isPlainObject(options)) {
            options = {};
        }
        options.url = url;
        options.method = 'POST';
        return Ajax(options);
    };

    Ajax.head = function(url, options)
    {
        if (!qw.isPlainObject(options)) {
            options = {};
        }
        options.url = url;
        options.method = 'HEAD';
        return Ajax(options);
    };

    Ajax.put = function(url, options)
    {
        if (!qw.isPlainObject(options)) {
            options = {};
        }
        options.url = url;
        options.method = 'PUT';
        return Ajax(options);
    };

    Ajax['delete'] = function(url, options)
    {
        if (!qw.isPlainObject(options)) {
            options = {};
        }
        options.url = url;
        options.method = 'DELETE';
        return Ajax(options);
    };

    return Ajax;
});
