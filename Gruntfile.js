module.exports = function(grunt) {
    grunt.initConfig({
        uglify: {
            dist: {
                files: {
                    'dist/ajax.min.js': ['index.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('others', 'Additional Tasks', function() {
        var fs = require('fs');
        fs.appendFileSync('dist/ajax.min.js', "\r\n");
        var data = fs.readFileSync('dist/ajax.min.js', {encoding: 'utf8'}).replace(/([a-z])\.delete/i, function(all, letter) {
            return letter + '[\'delete\']';
        });
        fs.writeFileSync('dist/ajax.min.js', data);
    });
    grunt.registerTask('default', 'Default Tasks', function() {
        grunt.task.run(['uglify:dist', 'others']);
    });
};
