<?php
date_default_timezone_set('GMT');
$method = $_SERVER['REQUEST_METHOD'];

$cors = false;
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
    $cors = true;
}

function is_local()
{
    return $_SERVER['REMOTE_ADDR'] === '127.0.0.1' || strpos($_SERVER['REMOTE_ADDR'], '192.168.') === 0;
}

if (isset($_GET['simulate_timeout'])) {
    sleep((int)$_GET['simulate_timeout']);
} elseif (is_local()) {
    usleep(500000);
}

if (isset($_GET['error']) && $_GET['error']) {
    header('HTTP/1.0 404 Not Found', 404);
    exit('Sayfa bulunamadı.');
}

if (isset($_GET['json'])) {
    header("Content-Type: application/json");
    exit(json_encode([
            'key 1' => 'value 1',
            'key 2' => 'value 2',
            'key 3' => 'value 3'
        ]));
}

if (isset($_GET['xml'])) {
    header("Content-Type: application/xml");
    exit('<?xml version="1.0"?>' . \PHP_EOL .
    '<root>' . \PHP_EOL .
    '   <item attr="attr value 1">Item 1</item>' . \PHP_EOL .
    '   <item attr="attr value 2">Item 2</item>' . \PHP_EOL .
    '   <item attr="attr value 3">Item 3</item>' . \PHP_EOL .
    '</root>' . \PHP_EOL);
}

$data = $method == 'GET' ? $_GET : $_POST;

$values = [];
foreach($data as $key => $value) {
    $values[] = $key . '=' . $value;
}

header("Content-Type: text/html;charset=UTF-8");
if (empty($values)) {
    echo 'no data sent';
} else {
    echo implode('<br>', $values);
}
