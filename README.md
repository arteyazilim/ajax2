
# Ajax.js #

### Ajax with method chain, detailed request object and with Cross-Origin-Request (CORS) support.

### Basit Kullanım:
```javascript
Ajax(ayarlar).done(fn).error(fn).always(fn);
```
#### Ayarlar:
    var defaultOptions = {
        // İstek methodu
        // şunlar olabilir: GET, POST, HEAD, PUT, DELETE
        // case sensitive'dir. varsayılan değeri GET'tir.
        method: 'GET',

        // İstek adresi
        url: '',

        // Sunucuya gönderilecek veridir.
        // Eğer istek methodu GET ya da HEAD ise script bu data verisini
        // sunucuya post etmek yerine istek adresine ekleyecektir.
        data: null,

        // İstek sonucunda dönen cevabın içerik türüdür.
        // şunlar olabilir: text, json, xml
        // case sensitive'dir. varsayılan değer 'text'tir.
        dataType: 'text',

        // zaman aşımı değeri.
        // eğer istek zaman aşımına uğrarsa -1 status koduya error methodu tetiklenir.
        // eğer 0 dan büyük değil ise bu değer girilmemiş sayılır.
        timeout: -1,

        // sunucuya gönderilecek başlıklardır.
        // eğer bir cors isteği tespit edilirse bu headerlar sunucuya gönderilmeyecektir.
        // şunlar olabilir: null ya da object literal
        headers: {'X-Requested-With': 'XMLHttpRequest'},

        // cacheleme durumunu belirler.
        // true ise eğer kesin cachlenecek diyemeyiz ama false ise kesin cachlenmez.
        // true ise url'nin sonuna her seferinde değişen bir query eklenir.
        // şunlar olabilir: true or false
        cache: false,

        // eğer cache false ise, "cacheQueryKey + '=' + random_value" şeklinde urlye ekleme yapılacaktır.
        // şunlar olabilir: null ya da string
        // eğer null girilmişse bu değer otomatik oluşturulacaktır.
        cacheQueryKey: null
    }

##### EK NOT: #####
Eğer global scope'ta **defaultAjaxOptions** adında bir değişken tanımlanmışsa, defaultOptions window.defaultAjaxOptions ile extend edilecektir.
Oluşturulan Ajax nesnesinin opsiyonları sırasıyla şunlardan oluştur: "defaultOptions", "window.defaultAjaxOptions", "kullanici_opsiyonlari".
__Bu opsiyonlardan sonra gelen öncekinin üzerine yazılır.__

#### Kısayollar
```javascript
Ajax.get(url [, options]);
// şu işlemin kısayoludur:
Ajax(extend({method: 'GET', url: url}, options);

Ajax.post(url [, options]);
// şu işlemin kısayoludur:
Ajax(extend({method: 'POST', url: url}, options);

Ajax.head(url [, options]);
// şu işlemin kısayoludur:
Ajax(extend({method: 'HEAD', url: url}, options);

Ajax.put(url [, options]);
// şu işlemin kısayoludur:
Ajax(extend({method: 'PUT', url: url}, options);

Ajax.delete(url [, options]);
// şu işlemin kısayoludur:
Ajax(extend({method: 'DELETE', url: url}, options);
```
#### Methodlar
Tüm methodlar zincirlenebilir.

    Ajax(opts)
        .done(fn)
        .error(fn)
        .always(fn)
        .abort([fn])

NOT: **done**, **error** ve **always** fonksiyonları kaydedildiği sıra ile tetiklenir.

##### **abort** methodu: #####
- abort methodu parametresiz olarak işletilirse request abort edilir.
- eğer bir fonksiyon parametresi ile işletilirse bu fonksiyon abort olayı olduğunda tetiklenmek üzere kaydedilir.
- abort methodu ile kaydedilmiş fonksiyona sadece 1 parametre aktarılır. o parametre Request nesnesidir.

##### **done**, **error** ve **always** methodlarıyla kaydedilmiş fonksiyonlar tetiklenirken bu fonksiyonlara aşağıdaki parametreler aktarılır: #####

1. JSON veya TEXT
2. Request Nesnesi
3. Response Nesnesi

Ek olarak error ile kaydedilmiş fonksiyona 5. bir parametre aktarılır. Bu parametre string türünde hata mesajıdır.

**[1] JSON veya TEXT :**
Cevap ile gelen veri `options.dataType` a göre parse edilir. Eğer parse başarısız olursa, script kayıtlı fonksiyonlara null aktarır.

**[2] Request Nesnesi :**
Methods:

1. **getXhr**     : XMLHttpRequest ya da XDomainRequest objesini döndürür.
2. **getMethod**  : İstek methodunu döndürür: GET|POST|HEAD|PUT|DELETE
3. **getUrl**     : İstek adresini döndürür.
4. **isAsync**    : istek asenkron mu diye bakar.
5. **getData**    : Post edilmiş parametreler ya da null.
6. **getHeaders** : Sunucuya gönderilmiş başlıkar veya null
7. **getTimeout** : Zaman aşımı değerini döndürür. -1 (dahil) ile +sonsuz arasındadır.
8. **getTime**    : İsteğin gönderildiği zamanı döndürür.


**[3] Response Nesnesi :**
Methods:

1. **xhr**     : XMLHttpRequest ya da XDomainRequest objesini döndürür.
2. **status**  : Status kodu döner. Timeout durumunda -1 döner. Hata durumunda ya da status okuyamama durumunda 0 döner.
3. **headers** : Cevaptaki başlıkları object literal veya null olarak döndürür.
4. **get**     : Cevaptaki Content-Type başlığına bakar JSON ya da TEXT döndürür.
5. **text**    : Content-Type ile ilgilenmeden cevap text olarak döndürür.
6. **json**    : Content-Type ile ilgilenmeden cevabı parse edip JSON ya da null döndürür.
7. **success** : Başarılı bir cevap mı diye bakar. Status kodu 200 (dahil) ile 300 (hariç) arasındaysa true döner.
8. **error**   : İstek başarısızsa bir hata mesajı döndürür.
